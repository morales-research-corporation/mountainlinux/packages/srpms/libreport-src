# Signature of the current package.
m4_define([AT_PACKAGE_NAME],
  [libreport])
m4_define([AT_PACKAGE_TARNAME],
  [libreport])
m4_define([AT_PACKAGE_VERSION],
  [2.9.5])
m4_define([AT_PACKAGE_STRING],
  [libreport 2.9.5])
m4_define([AT_PACKAGE_BUGREPORT],
  [crash-catcher@fedorahosted.org])
m4_define([AT_PACKAGE_URL],
  [])
